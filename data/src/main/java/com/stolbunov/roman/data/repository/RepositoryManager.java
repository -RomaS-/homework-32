package com.stolbunov.roman.data.repository;

import com.stolbunov.roman.data.mapper.NoteMapper;
import com.stolbunov.roman.data.repository.local_db.DBNoteRepository;
import com.stolbunov.roman.domain.entity.Note;
import com.stolbunov.roman.domain.repository.INoteRepositoryBoundary;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;

public class RepositoryManager implements INoteRepositoryBoundary {
    private IDBNote db;

    @Inject
    RepositoryManager(DBNoteRepository db) {
        this.db = db;
    }

    @Override
    public Completable add(Note note) {
        return Single.just(note)
                .map(NoteMapper::transform)
                .flatMapCompletable(db::add);
    }

    @Override
    public Completable remove(long noteId) {
        return Single.just(noteId)
                .flatMapCompletable(db::remove);
    }

    @Override
    public Completable update(Note note) {
        return Single.just(note)
                .map(NoteMapper::transform)
                .flatMapCompletable(db::update);
    }

    @Override
    public Single<List<Note>> getAllNotes() {
        return db.getAllNotes()
                .map(NoteMapper::transformList);
    }

    @Override
    public Single<Note> getNote(long noteId) {
        return Single.just(noteId)
                .flatMap(db::getNote)
                .map(NoteMapper::transform);
    }
}
