package com.stolbunov.roman.homework_32.ui.activity.note_editor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.stolbunov.roman.domain.entity.Note;
import com.stolbunov.roman.homework_32.R;
import com.stolbunov.roman.homework_32.ui.activity.Event;
import com.stolbunov.roman.homework_32.ui.activity.note_list.MainActivity;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import dagger.android.AndroidInjection;

public class NoteEditorActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String KEY_INTENT_NOTE = "KEY_INTENT_NOTE";
    private long noteId;

    private NoteEditorViewModel vm;

    private AppCompatEditText title;
    private AppCompatEditText description;
    private AppCompatTextView showPriority;
    private Toolbar toolbar;

    @Inject
    NoteEditModelFactory viewModelFactory;

    public static Intent getIntent(Context context, long noteId) {
        Intent intent = new Intent(context, NoteEditorActivity.class);
        intent.putExtra(KEY_INTENT_NOTE, noteId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_editor);

        initView();
        initViewModel();
        setSupportActionBar(toolbar);

        getDataWithIntent();

        observerShowPriority();
        observerEditableNote();
        observerUpdatedNoteId();
    }

    private void getDataWithIntent() {
        Intent intent = getIntent();
        noteId = intent.getLongExtra(KEY_INTENT_NOTE, -1);

        if (noteId > -1) {
            vm.getNote(noteId);
        }
    }

    private void initView() {
        title = findViewById(R.id.note_editor_title);
        description = findViewById(R.id.note_editor_description);
        showPriority = findViewById(R.id.note_editor_show_priority);
        toolbar = findViewById(R.id.edit_bottom_app_bar);

        findViewById(R.id.note_editor_high_priority).setOnClickListener(this);
        findViewById(R.id.note_editor_medium_priority).setOnClickListener(this);
        findViewById(R.id.note_editor_low_priority).setOnClickListener(this);
        findViewById(R.id.note_editor_fab).setOnClickListener(this);
    }

    private void initViewModel() {
        vm = ViewModelProviders
                .of(this, viewModelFactory)
                .get(NoteEditorViewModel.class);
    }

    private void filingFields(Event<Note> event) {
        if (!event.isHandled()) {
            event.setHandled(true);
            title.setText(event.getData().getTitle());
            title.setSelection(title.length());
            description.setText(event.getData().getDescription());
            showPriority.setText(event.getData().getPriority().name());
        }
    }

    private void observerEditableNote() {
        vm.observeEditableNote().observe(this, this::filingFields);
    }

    private void observerShowPriority() {
        vm.observeSelectedPriority().observe(this, showPriority::setText);
    }

    private void observerUpdatedNoteId() {
        vm.observeUpdatedNoteId().observe(this, this::editNote);
    }

    private void editNote(Event<Boolean> isSuccess) {
        if (isSuccess.isHandled()) {
            isSuccess.setHandled(true);

            Intent intent = MainActivity.getIntent(this);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.note_editor_high_priority:
                vm.showSelectedPriority(R.string.high);
                break;
            case R.id.note_editor_medium_priority:
                vm.showSelectedPriority(R.string.medium);
                break;
            case R.id.note_editor_low_priority:
                vm.showSelectedPriority(R.string.low);
                break;
            case R.id.note_editor_fab:
                vm.update(createNewNote());
                break;
        }
    }

    private Note createNewNote() {
        return new Note(
                noteId,
                title.getText().toString(),
                description.getText().toString(),
                showPriority.getText().toString());
    }
}
