package com.stolbunov.roman.homework_32.ui.widgets;

import android.content.Context;
import android.content.res.ColorStateList;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.stolbunov.roman.homework_32.R;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

public class CustomPriorityImageView extends FrameLayout {
    private AppCompatImageView centerImage;
    private AppCompatImageView indicatorImage;

    public CustomPriorityImageView(@NonNull Context context) {
        this(context, null);
    }

    public CustomPriorityImageView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomPriorityImageView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.widget_priority_note, this, true);

        centerImage = view.findViewById(R.id.custom_view_center_image);
        indicatorImage = view.findViewById(R.id.custom_view_indicator_image);
    }

    public void setCenterImageResource(@DrawableRes int resId) {
        centerImage.setImageResource(resId);
    }

    public void setBackgroundTintList(ColorStateList tint) {
        indicatorImage.setBackgroundTintList(tint);
    }
}
