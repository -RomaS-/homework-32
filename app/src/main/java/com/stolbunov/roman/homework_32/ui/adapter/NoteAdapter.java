package com.stolbunov.roman.homework_32.ui.adapter;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stolbunov.roman.domain.entity.Note;
import com.stolbunov.roman.homework_32.R;
import com.stolbunov.roman.homework_32.ui.widgets.CustomPriorityImageView;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteViewHolder> {
    private List<Note> data;
    private OnRemoveNoteListener removeListener;
    private OnItemClickListener itemClickListener;

    @Inject
    public NoteAdapter() {
        data = Collections.emptyList();
    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int itemType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_note_main, viewGroup, false);
        return new NoteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteViewHolder noteViewHolder, int position) {
        noteViewHolder.bind(getNoteByPosition(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<Note> notes) {
        data = notes;
        Collections.sort(this.data);
        notifyDataSetChanged();
    }

    private Note getNoteByPosition(int position) {
        return data.get(position);
    }

    public void setRemoveListener(OnRemoveNoteListener removeListener) {
        this.removeListener = removeListener;
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    class NoteViewHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView title;
        private AppCompatTextView description;
        private AppCompatImageButton remove;
        private CustomPriorityImageView priority;


        NoteViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.rv_note_title);
            description = itemView.findViewById(R.id.rv_note_description);
            remove = itemView.findViewById(R.id.rv_note_remove);
            priority = itemView.findViewById(R.id.rv_note_priority);
        }

        void bind(Note note) {
            remove.setOnClickListener(v -> onRemoveClick(note));
            itemView.setOnClickListener(v -> onItemClick(note));

            title.setText(note.getTitle());
            description.setText(note.getDescription());
            fillingPriority(note);
        }

        private void fillingPriority(Note note) {
            switch (note.getPriority()) {
                case HIGH:
                    priority.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                    priority.setCenterImageResource(R.drawable.ic_priority_high);
                    break;
                case MEDIUM:
                    priority.setBackgroundTintList(ColorStateList.valueOf(Color.YELLOW));
                    priority.setCenterImageResource(R.drawable.ic_priority_medium);
                    break;
                case LOW:
                    priority.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
                    priority.setCenterImageResource(R.drawable.ic_priority_low);
                    break;
            }
        }

        private void onItemClick(Note note) {
            if (itemClickListener != null) {
                itemClickListener.onItemClick(note.getId());
            }
        }

        private void onRemoveClick(Note note) {
            if (removeListener != null) {
                removeListener.onRemoveNote(note);
            }
        }
    }

    public interface OnRemoveNoteListener {
        void onRemoveNote(Note note);
    }
}