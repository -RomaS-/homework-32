package com.stolbunov.roman.homework_32.ui.activity.note_list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.stolbunov.roman.domain.entity.Note;
import com.stolbunov.roman.homework_32.R;
import com.stolbunov.roman.homework_32.ui.activity.Event;
import com.stolbunov.roman.homework_32.ui.activity.note_editor.NoteEditorActivity;
import com.stolbunov.roman.homework_32.ui.adapter.NoteAdapter;
import com.stolbunov.roman.homework_32.ui.adapter.OnItemClickListener;
import com.stolbunov.roman.homework_32.ui.fragment.AddNewNoteDialogFragment;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dagger.android.support.DaggerAppCompatActivity;

public class MainActivity extends DaggerAppCompatActivity implements
        AddNewNoteDialogFragment.OnSaveDataClickListener, OnItemClickListener,
        NoteAdapter.OnRemoveNoteListener, View.OnClickListener {

    private final static int RC_NOTE_EDIT = 351;

    private View containerProgress;
    private View containerList;
    private RecyclerView recyclerView;

    @Inject
    NoteAdapter adapter;
    @Inject
    NoteListViewModelFactory viewModelFactory;

    private NoteListViewModel vm;

    public static Intent getIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomAppBar bottomAppBar = findViewById(R.id.bottom_app_bar);
        setSupportActionBar(bottomAppBar);

        containerProgress = findViewById(R.id.container_progress);
        containerList = findViewById(R.id.container_list);
        recyclerView = findViewById(R.id.rv_notes);

        FloatingActionButton fab = findViewById(R.id.fab_note_list);
        fab.setOnClickListener(this);

        initNoteAdapter();
        initNoteRecyclerView();

        initViewModel();
        observerNewDataList();
        observerShowProgress();
        observerStartEditNoteActivity();
    }

    @Override
    public void onSaveClick(Note note) {
        vm.add(note);
    }

    @Override
    public void onRemoveNote(Note note) {
        vm.remove(note);
    }

    @Override
    public void onItemClick(long noteId) {
        vm.startEditNoteActivity(noteId);
    }

    @Override
    public void onErrorSaveClick(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_note_list:
                AddNewNoteDialogFragment dialogFragment = AddNewNoteDialogFragment.getInstance();
                dialogFragment.show(getSupportFragmentManager(), "AddNewNoteDialogFragment");
        }
    }

    private void initNoteAdapter() {
        adapter.setItemClickListener(this);
        adapter.setRemoveListener(this);
    }

    @NonNull
    private LinearLayoutManager getManager() {
        return new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
    }

    private void initNoteRecyclerView() {
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(getManager());
    }

    private void initViewModel() {
        vm = ViewModelProviders
                .of(this, viewModelFactory)
                .get(NoteListViewModel.class);
    }

    private void observerNewDataList() {
        vm.observeAllNotes().observe(this, adapter::setData);
    }

    private void observerShowProgress() {
        vm.observeShowProgress().observe(this, isShow -> {
            if (isShow) {
                containerList.setVisibility(View.GONE);
                containerProgress.setVisibility(View.VISIBLE);
            } else {
                containerList.setVisibility(View.VISIBLE);
                containerProgress.setVisibility(View.GONE);
            }
        });
    }

    private void observerStartEditNoteActivity() {
        vm.observeStartEditNoteActivity().observe(this, this::goToEditor);
    }

    public void goToEditor(Event<Long> event) {
        if (!event.isHandled()) {
            event.setHandled(true);
            Intent intent = NoteEditorActivity.getIntent(this, event.getData());
            startActivityForResult(intent, RC_NOTE_EDIT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RC_NOTE_EDIT:
                    vm.getAllNotes();
                    break;
            }
        }
    }
}
