package com.stolbunov.roman.homework_32.di;

import com.stolbunov.roman.homework_32.ui.activity.note_editor.NoteEditorActivity;
import com.stolbunov.roman.homework_32.ui.activity.note_list.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Module(includes = AndroidSupportInjectionModule.class)
interface DaggerAndroidModule {

    @ContributesAndroidInjector
    MainActivity mainActivity();

    @ContributesAndroidInjector()
    NoteEditorActivity noteEditorActivity();
}
