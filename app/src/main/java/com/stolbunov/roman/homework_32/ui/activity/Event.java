package com.stolbunov.roman.homework_32.ui.activity;

public class Event<T> {
    private T data;
    private boolean isHandled;

    public Event(T data) {
        this.data = data;
        isHandled = false;
    }

    public T getDataIfNotHandled() {
        if (isHandled) {
            return null;
        } else {
            isHandled = true;
            return data;
        }
    }

    public T peekData() {
        return data;
    }

    public void setHandled(boolean handled) {
        isHandled = handled;
    }

    public boolean isHandled() {
        return isHandled;
    }

    public T getData() {
        return data;
    }
}
