package com.stolbunov.roman.homework_32.ui.activity.note_editor;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class NoteEditModelFactory implements ViewModelProvider.Factory {
    private NoteEditorViewModel vm;

    @Inject
    NoteEditModelFactory(NoteEditorViewModel vm) {
        this.vm = vm;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) vm;
    }
}
