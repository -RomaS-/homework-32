package com.stolbunov.roman.homework_32.ui.activity.note_list;

import android.util.Log;

import com.stolbunov.roman.domain.entity.Note;
import com.stolbunov.roman.domain.use_case.IUseCase;
import com.stolbunov.roman.homework_32.ui.activity.Event;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class NoteListViewModel extends ViewModel {
    private IUseCase useCase;
    private CompositeDisposable composite = new CompositeDisposable();

    private MutableLiveData<List<Note>> allNotes = new MutableLiveData<>();
    private MutableLiveData<Boolean> isShowProgress = new MutableLiveData<>();
    private MutableLiveData<Event<Long>> startEditNoteActivity = new MutableLiveData<>();

    @Inject
    public NoteListViewModel(IUseCase useCase) {
        this.useCase = useCase;
        getAllNotes();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        composite.dispose();
    }

    LiveData<List<Note>> observeAllNotes() {
        return allNotes;
    }

    LiveData<Boolean> observeShowProgress() {
        return isShowProgress;
    }

    LiveData<Event<Long>> observeStartEditNoteActivity() {
        return startEditNoteActivity;
    }

    void add(Note note) {
        composite.add(useCase.add(note)
                .subscribeOn(Schedulers.io())
                .subscribe(this::getAllNotes, this::logger));
    }

    void remove(Note note) {
        composite.add(useCase.remove(note.getId())
                .subscribeOn(Schedulers.io())
                .subscribe(this::getAllNotes, this::logger));
    }

    void getAllNotes() {
        composite.add(useCase.getNoteList()
                .doOnSubscribe(disposable -> isShowProgress.postValue(true))
                .doOnSuccess(list -> isShowProgress.postValue(false))
                .subscribeOn(Schedulers.io())
                .subscribe(allNotes::postValue, this::logger));
    }

    void startEditNoteActivity(long noteId) {
        startEditNoteActivity.postValue(new Event<>(noteId));
    }

    private void logger(Throwable throwable) {
        Log.e("ALevel", throwable.getMessage());
    }
}
