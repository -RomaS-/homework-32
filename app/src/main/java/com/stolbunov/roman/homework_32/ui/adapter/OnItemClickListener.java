package com.stolbunov.roman.homework_32.ui.adapter;

public interface OnItemClickListener {
    void onItemClick(long noteId);
}
