package com.stolbunov.roman.domain.use_case;

import com.stolbunov.roman.domain.entity.Note;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface IUseCase {
    Completable add(Note note);

    Completable remove(long noteId);

    Completable update(Note note);

    Single<Note> getNote(long noteId);

    Single<List<Note>> getNoteList();
}
