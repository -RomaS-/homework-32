package com.stolbunov.roman.domain.repository;

import com.stolbunov.roman.domain.entity.Note;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface INoteRepositoryBoundary {
    Completable add(Note note);

    Completable remove(long noteId);

    Completable update(Note note);

    Single<List<Note>> getAllNotes();

    Single<Note> getNote(long noteId);
}
